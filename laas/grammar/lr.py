"""
    Context-free grammars. LR0/SLR1/LALR1 parsing tools
    (cc) CC BY-NC-SA Creative Commons -  Bruno.Bogaert@univ-lille.fr
"""
import itertools

#from .grammar import Grammar

from .common import GRAMMAR_START
from .common import EOD
from .common import Symbol
from .common import Rule
from .common import Word
from .common import ConflictError
from .common import ParseError
from .common import Parser
from .semantic import RuleRun
from .semantic import Semantic


from collections import deque
from collections import defaultdict

from types import SimpleNamespace
from enum import Enum

# from .semantic import Semantic

from graphviz import Source


class LR0Item() :
    """
        tuple : (left_var, right_word, point_position)
    """

    def __init__(self, rule, point_pos) :
        """

            private instance attributes :
                _rule : base rule
                _point_pos : point position
                _grammar : related grammar (Grammar instance)
                _follow_point : symbol just following point position
            public instance attributes :
        """
        # assert len(t) == 3
        left, right = rule
        assert point_pos >= 0 and point_pos <= len(right), f"/{left}/{right}/{point_pos}/" # len(right)+1 positions for separator
        self._rule = rule
        self._point_pos = point_pos


    def __hash__(self) :
        return (self._rule, self.point_pos).__hash__()
    def __eq__(self ,other) :
        return (self._rule, self.point_pos).__eq__((other._rule, other.point_pos))
    def __str__(self) :
        k = self.point_pos
        before = Word(*self.right[0:k]) if k>0  else ''
        after =  Word(*self.right[k:]) if k<len(self.right) else ''
        return f'{self.left}  \N{RIGHTWARDS ARROW WITH TAIL}  {before} \u2022 {after}'
    def __repr__(self) :
        return self.__str__()

    @property
    def left(self) -> Symbol :
        """
            left part of the rule (variable symbol)
        """
        return self._rule.left
    @property
    def right(self) -> Word :
        """
            right part of the rule (sequence of symbols)
        """
        return self._rule.right
    @property
    def base_rule(self) -> Rule :
        """
            right part of the rule (sequence of symbols)
        """
        return self._rule
    @property
    def point_pos(self) -> int :
        """
            point position (between 0 an len(right part))
        """
        return self._point_pos
    @property
    def follow_point(self) -> Symbol :# | None:
        """
            symbol following  point.
            None if point is at last position
        """
        try :
            return self.right[self.point_pos]
        except IndexError :
            return None

    def is_reduce_action_rule(self) -> bool :
        return self.point_pos == len(self.right) and self.left != GRAMMAR_START

    def is_not_ending_point_rule(self) -> bool :
        return self.point_pos < len(self.right)

    def saturation_step(self, grammar) -> tuple :
        """
            computes first step closure of this pointed rule
                grammar : related grammar
                return :  list [LR0Item]
        """
        if not grammar.isVar(self.follow_point) :
            return tuple()
        else :
            return tuple(
                LR0Item(rule=r, point_pos=0)
                for r in grammar.byVar[self.follow_point]
            )

    def shift(self) :
        """
            create shifted rule
            param : grammar:
            return : LR0Item, shifting point to right
        """
        return LR0Item(rule = self._rule, point_pos=self.point_pos + 1)


class LR0State(tuple) :
    def __new__(cls, kernel : tuple, grammar, id : int) :
        """
            LR0State  : tuple [LR0Item] (in construction order, so tuple begins with kernel)
            kernel :  tuple [LR0Item]
            grammar : related grammar
        """
        assert type(kernel) is tuple
        rules = [*kernel]
        variables = set()
        todo = deque(kernel)
        while todo :
            rule = todo.pop()
            symbol = rule.follow_point
            if symbol is not None and grammar.isVar(symbol) and symbol not in variables :
                variables.add(symbol)
                for new_rule in rule.saturation_step(grammar) :
                    rules.append(new_rule)
                    todo.append(new_rule)
        return super().__new__(cls, rules)
        # todo = deque(kernel)
        # while todo:
        #     rule = todo.pop()
        #     for sat_r in rule.saturation_step(grammar):
        #         if sat_r not in rules_dict:
        #             rules_dict[sat_r] = True
        #             todo.append(sat_r)
        # return super().__new__(cls, rules_dict)

    def __init__(self, kernel : tuple, grammar , id : int):
        self._kernel_card = len(kernel)
        self._grammar = grammar
        self._id = id
        # print(f'created {self}')
        self._reduce_action_rules = \
            tuple(r for r in self if r.is_reduce_action_rule())
        # compute transitions:
        trans = defaultdict(list)  # key : symbol, value : list of LR0Item
        for rule in self :
            if rule.is_not_ending_point_rule() :
                symbol = rule.follow_point
                trans[symbol].append(rule.shift())
        # same dict without default and replacing lists by tuples,
        # so self._transitions :  dict[Symbol,tuple[LR0Item]]
        self._transitions = { x : tuple(l) for x ,l in trans.items() }

    @property
    def kernel(self) -> tuple[LR0Item] :
        """
            state kernel (tuple)
        """
        return self[:self._kernel_card]
    @property
    def id(self) -> int :
        return self._id
    @property
    def transitions(self) -> dict[Symbol,tuple[LR0Item]] :
        return self._transitions

    def reduce_rules(self) :
        '''
            returns tuple of "reduce_action" rules
        '''
        return self._reduce_action_rules

    def __str__(self) -> str :
        bs = self.kernel
        return f'state {self.id}\n' \
                + '\n'.join('  '+ str(r) for r in bs) \
                + '\n   ---\n' \
                + '\n'.join('  ' + str(r) for r in self[len(bs):])


class LR0Automaton():
    """
        LR0 automaton
        private instance attributes :
            _grammar : related grammar (Grammar instance)
        public instance attributes :
            states : states tuple, ordered by index
            kernels : dictionnary
                key : state kernel (tuple of LR0Item)
                value : state (LR0State instance)
            transitions : dict (key : state index) of dict(key : symbol, value : state instance)
    """
    def __init__(self, grammar):
        self._grammar = grammar
        self._kernels = dict()  # key : kernel, value : state instance
        self._createAutomaton()
        self._states = tuple(self._kernels.values())  # states, ordered by index
        self._transitions = {
            i: { letter:self._kernels[dest] for letter, dest in state.transitions.items()}
            for i, state in enumerate(self.states)
        }

    def _ensureState(self, kernel) -> (LR0State,bool):
        """
            search or create a state for this kernel
            returns tuple ( state, is_new (boolean) )
        """
        try:
            return self._kernels[kernel], False
        except KeyError:  # kernel unknown
            state = LR0State(kernel, grammar = self._grammar, id= len(self._kernels))  # build state
                # new state, record it
            self._kernels[kernel] = state  # record kernel
            return state, True

    def _createAutomaton(self):
        '''
            Creates states. Updates self._kernels dict
        '''
        initial_kernel = (LR0Item( rule = self._grammar.starting_rule, point_pos= 0 ), ) #tuple
        initial_state, _ = self._ensureState(initial_kernel)
        todo = deque([initial_state])  #
        while todo:
            state = todo.popleft()
            # print (f'treating state {index[state]} : {state}')
            for kernel in state.transitions.values():
                dest, is_new = self._ensureState(kernel)
                if is_new:
                    todo.append(dest)


    @property
    def states(self):
        """
            tuple of automaton states, ordered by index
        """
        return self._states
    @property
    def kernels(self):
        """
            dictionary
                key : kernel
                value : state instance
        """
        return self._kernels
    @property
    def transitions(self):
        """
            dictionary
                key : state index
                value : dictionary
                    key : symbol,
                    value : LR0state
            e.g : transitons[i][s] -> state
        """
        return self._transitions

    def __str__(self) ->str :
        return '\n'.join(str(state) for state in self.states)


    def to_dot(self):
        '''
            return DOT (graphviz source text) automaton representation
        '''
        rules_list = lambda state : '\n'.join(map(lambda item : f'{item}<br align="left"/>',state))
        node_state = lambda state: f'{state.id} [id="{state.id}", label=<<font color="red"><b>{state.id}</b></font><br/>{rules_list(state)}>\n]'
        make_trans = lambda from_index, to_index, letter: f' {from_index}->{to_index} [label=<<i>{letter}</i>>]'
        transitions = ((from_index, dest[letter].id, letter) for from_index, dest in self.transitions.items() for letter in dest)
        return '\n'.join(itertools.chain(
            ('digraph AutLR0 {','bgcolor=transparent','rankdir=LR','node[shape=box,style=rounded]'),
            map(node_state, self.states),
            itertools.starmap(make_trans,transitions),
            ('}',)
        ))


    def to_gv(self):
        '''
            return graphviz object
        '''
        return Source(self.to_dot())

    def _repr_svg_(self):
        return Source(self.to_dot(),format='svg').pipe(encoding='utf-8')


    def table(self, mode='LR0', raise_exception = True) :
        """
            Builds and returns LRTable (either LR0 or SLR1)
        """
        return LRTable(self,mode,raise_exception)

    def _is_mode(self,mode):
        assert mode in ('LR0','SLR1','LALR1')
        try:
            self.table(mode, raise_exception=True)
            return True
        except:
            return False

    def isLR0(self):
        return self._is_mode('LR0')

    def isSLR1(self):
        return self._is_mode('SLR1')

    def isLALR1(self):
        return self._is_mode('LALR1')


class LRTable(dict) :
    """
        dictionary
        keys : tuple: (state_id, symbol)
        values : tuple (Action|None, action argument :int|Rule )
        e.g. t[(3,'a')] = (Action.S, 6)
    """
    class Action(Enum):
        S = 'Shift'
        R = 'Reduce'
        A = 'Accept'
        def __str__(self):
            return self.name

    def __init__(self, automaton , mode = 'SLR1', raise_exception = True):
        """
            states_ids  :  Iterable[int]
            grammar : Grammar
            raise_exception : bool
        """
        super().__init__()
        self._states_ids = range(len(automaton.states))
        self._grammar = automaton._grammar
        self._terminals = (*automaton._grammar.terminals, EOD) # Iterable[Symbol]
        self._variables = automaton._grammar.variables         # Iterable[Symbol]
        self._raise_exception = raise_exception

        self._symbols = { *self._terminals, *self._variables }

        self._conflicts = defaultdict(list)

        if mode == 'LALR1' :
            preds = LALR1Predictive(automaton)

        reduce_letters_functions = {
            'LR0': lambda _,__: list(self._grammar.terminals) + [EOD],
            'SLR1': lambda r,_: self._grammar.suiv[r.left] if r.left != GRAMMAR_START else EOD,
            'LALR1': lambda r,state_id : preds[state_id][r]
        }
        reduce_letters_set = reduce_letters_functions[mode]
        table = self
        accepting_state = automaton.transitions[0][automaton._grammar.axiom]
        table[(accepting_state.id, EOD)] = (LRTable.Action.A, '')
        for state in automaton.states:
            for r in state.reduce_rules() :
                for letter in reduce_letters_set(r,state.id):
                    table[(state.id, letter)] = (LRTable.Action.R, r.base_rule)
            for letter, dest in automaton.transitions[state.id].items():
                if automaton._grammar.isVar(letter) :
                    table[(state.id, letter)] = (None, dest.id)
                else :
                    table[(state.id, letter)] = (LRTable.Action.S, dest.id)

    def _conflict_message(self, state_id, letter, old_val, new_val):
        old_type, old_action = old_val
        new_type, new_action = new_val
        conflict_name = f'{old_type.value}/{new_type.value}'
        return f'conflict {conflict_name}, state {state_id}, letter {letter}\n {old_type} : {old_action}\n {new_type} : {new_action}\n'

    def get_conflicts_messages(self):
        return [ self._conflict_message(*conflict) for conflict in self.get_conflicts() ]

    def get_conflicts(self):
        return [ (state, letter, self[(state,letter)],elt)
                 for (state,letter),liste in self._conflicts.items()
                 for elt in liste
               ]

    def has_conflict(self):
        return bool(self._conflicts)

    def parser(self):
        return LRParser(self)

    def __setitem__(self, key, new_val):
        # print('set item', key,new_val)
        assert type(key) is tuple and len(key) == 2 \
               and key[0] in self._states_ids and key[1] in self._symbols, f'{key} is not (state_id, symbol)'
        assert type(new_val) is tuple and len(new_val) == 2
        new_action, new_arg = new_val
        assert key[1] in self._variables and new_action is None \
            or key[1] in self._terminals and type(new_action) is LRTable.Action, f'wrong value {new_action} {new_arg}'

        if key in self : #conflict
            self._conflicts[key].append(new_val)
            if self._raise_exception :
                raise ConflictError(self._conflict_message(key[0],key[1],self[key],new_val))
        else :
            dict.__setitem__(self, key, new_val)

    def to_pandas(self):
        if self.has_conflict() :
            raise ConflictError("this table has conflicts. Use .get_conflicts() method")
        import pandas
        df = pandas.DataFrame(dict.fromkeys(self._states_ids, ''), index = [*self._terminals, *self._variables])
        for (state, symbol), (t,a) in self.items():
            t = (t or '') and f'[{t}]'
            df[state][symbol] = f'{t}{a}'
        return df

    def to_latex(self):
        df = self.to_pandas()
        return  df.style.to_latex(column_format=f'|c|*{{{len(df.columns)}}}{{c|}}', hrules=True)

    def to_html(self):
        styler = self.to_pandas().style
        td = {'selector': 'td', 'props': 'border : solid 0.8pt; text-align:left'}
        th = {'selector':'th','props': 'text-align:center'}
        styler.set_table_styles({
            '#' : [{'selector': 'td,th', 'props': 'border-bottom : solid 3pt darkgrey'}, ]
        }, overwrite=False, axis=1)
        styler.set_table_styles([td, th],overwrite=False)
        return styler.to_html(hrules=True)

    def to_markdown(self):
        df = self.to_pandas()
        return df.to_markdown()

    def _repr_html_(self):
        return self.to_html()

class RunManager() :
    def __init__(self, semantic : Semantic):
        self._stack = []
        self._token_semantic = semantic.token_semantic()
        self._semantic = semantic

    def create_run(self, rule:Rule):
        created = RuleRun(rule=rule, semantic=self._semantic)
        if len(rule.right)>0 :
            created.add_results(* self._stack[-len(rule.right):])
            del self._stack[-len(rule.right):]
        if created.isComplete() :
            self.add_result(created.synthetised)

    def add_token_result(self,value):
        self.add_result(self._token_semantic(value))

    def add_result(self, value):
        self._stack.append(value)

class LRParser(Parser) :
    def __init__(self, table : LRTable):
        self._table = table

    def parse_tokens(self, tokens, verbose=False, semantic: Semantic = None):
        """
            Tokens parsing
            tokens : Iterable[Token] (without End Of Data token)
            Token :  any class with 'type' and 'value' attributes
            semantic : Instance de Semantic (actions sémantiques associées) ou None
            Return : True if semantic is None or else axiom synthetised attribute
        """
        table = self._table
        input = itertools.chain(tokens, [SimpleNamespace(type=EOD,value=None)])
        current = next(input)
        stack = [0]
        if semantic is not None :
            sem_manager = RunManager(semantic)
        action_type , action_arg = table.get((stack[-1], current.type), (None, None))
        verbose and print(f'stack:{stack}, current: {current.type}')
        while action_type != LRTable.Action.A :
            if action_type == LRTable.Action.S :
                if semantic is not None:
                    sem_manager.add_token_result(current)
                stack.append(action_arg)
                current = next(input)
            elif action_type == LRTable.Action.R :
                if semantic is not None:
                    sem_manager.create_run(action_arg)
                if len(action_arg.right)>0 :
                    del stack[-len(action_arg.right):]
                _, dest = table[(stack[-1],action_arg.left)]
                stack.append(dest)
            else :
                raise ParseError(f"Syntax Error. action_type : {action_type} stack : {stack} current : {current.type}  {list(input)}")
            action_type , action_arg = table.get((stack[-1], current.type), (None, None))
            verbose and print(f'stack:{stack}, current: {current.type}')
        if semantic is not None:
            sem_manager.create_run(self._table._grammar.starting_rule)
            return sem_manager._stack[0]
        else :
            return True


class LALR1Predictive(tuple) :
    """
        tuple (ordered by state ids) of dictionaries LR0_Item => set of predictive symbols
        e.g :
           pred[5][LR0Item(V->u.v)] is predictive symbol list for V->u.v in state 5
        Iterative version
    """
    def __new__(cls, automaton : LR0Automaton):
        todo = deque()
        registered = tuple( {item:{} for item in state} for state in automaton.states )
        cf = {r: automaton._grammar._context_follows(r.right) \
                        for r in ( *automaton._grammar.rules, automaton._grammar.starting_rule ) }

        def add_symbol(state_id : int, item : LR0Item, symbol : Symbol) :
            if symbol not in registered[state_id][item] :
                already_visited = bool(registered[state_id][item])
                registered[state_id][item].update({symbol:None})
                todo.append((state_id,item,symbol,already_visited))

        def cascade(state_id : int, item : LR0Item, symbol : Symbol, already_visited) :
                # saturation :
                if item.point_pos in cf[item.base_rule] : # variable
                    # if not already_visited :
                    #     print(f'first visit for {state_id}-{item}')
                    first = () if already_visited else cf[item.base_rule][item.point_pos].letters
                    inherit = cf[item.base_rule][item.point_pos].eps * (symbol,)
                    for new_symbol in (*first,*inherit) :
                        for sat_item in item.saturation_step(automaton._grammar):
                            add_symbol(state_id,sat_item,new_symbol)
                # transition :
                if item.is_not_ending_point_rule() :
                    dest_state_id = automaton.transitions[state_id][item.follow_point].id
                    add_symbol(dest_state_id, item.shift(), symbol)

        add_symbol(0, automaton.states[0][0], EOD)
        while todo :
            cascade( * todo.popleft() )

        return super().__new__(cls,  registered)

    def display(self):
        for state_id, pred_dict in enumerate(self) :
            print(f'== state {state_id} ==')
            for item, pred in pred_dict.items() :
                print(f'  {item} , {list(pred.keys())}')



class LALR1PredictiveR(tuple) :
    """
        tuple (ordered by state ids) of dictionaries LR0_Item => set of predictive symbols
        e.g :
           pred[5][LR0Item(V->u.v)] is predictive symbol list for V->u.v in state 5
        Recursive version
    """
    def __new__(cls, automaton : LR0Automaton):
        registered = tuple( {item:{} for item in state} for state in automaton.states )
        cf = {r: automaton._grammar._context_follows(r.right) \
                        for r in ( *automaton._grammar.rules, automaton._grammar.starting_rule ) }

        def add_symbol(state_id : int, item : LR0Item, symbol : Symbol) :
            if symbol not in registered[state_id][item] :
                already_visited = bool(registered[state_id][item])
                registered[state_id][item].update({symbol:None})
                # saturation :
                if item.point_pos in cf[item.base_rule] : # variable
                    first = () if already_visited else cf[item.base_rule][item.point_pos].letters
                    inherit = cf[item.base_rule][item.point_pos].eps * (symbol,)
                    for new_symbol in (*first,*inherit) :
                        for sat_item in item.saturation_step(automaton._grammar):
                            add_symbol(state_id,sat_item,new_symbol)
                # transition :
                if item.is_not_ending_point_rule() :
                    dest_state_id = automaton.transitions[state_id][item.follow_point].id
                    add_symbol(dest_state_id, item.shift(), symbol)

        add_symbol(0, automaton.states[0][0], EOD)

        return super().__new__(cls,  registered)

    def display(self):
        for state_id, pred_dict in enumerate(self) :
            print(f'== state {state_id} ==')
            for item, pred in pred_dict.items() :
                print(f'  {item} , {pred}')

