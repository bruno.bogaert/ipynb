"""
    Context-free grammars.
    (cc) CC BY-NC-SA Creative Commons -  Bruno.Bogaert@univ-lille.fr
"""
from collections import defaultdict
from collections import namedtuple
# from functools import total_ordering

from .common import EOD
from .common import EPSILON
from .common import GRAMMAR_START
from .common import PATTERNS
from .common import Symbol
from .common import Word
from .common import Rule

from .ll1 import LL1Table
from .lr import LR0Automaton
from .lr import LRTable

from .semantic import DictSemantic

import itertools

from typing import Iterable
from typing import Union

WordFollow =  namedtuple('WordFollow', ['letters', 'eps'])

class Grammar () :
    """ Context free grammar
    Attributes and pseudo attributes :
        terminals : set of terminals (Symbols)
        variables : set of variables (Symbols)
        axiom : axiom (Symbol)
        rules : rules iterator
        eps_prod : "epsilon-productive" property (vars : set of variables, rules : set of rules)
        productive : "productive property" (vars : set of variables, rules : set of rules)
        accessible : set of accessible variables
        prem : "First" sets  (dict of sets indexed by variables and rules)
        suiv : "Follow" sets  (dict of sets indexed by variables)

    """

    def __repr__(self):
        return f'''{self.__class__.__name__}(
 terminals : {' '.join(self.terminals)}
 variables : {' '.join(self.variables)}
 axiom : {self.axiom}
 rules : {list(str(r) for r in self.rules)}
)'''
    def to_compact_string(self):
        return '\n'.join(f'{left} -> {" | ".join(str(w) for v,w in rules)}' for left,rules in self.byVar.items())
    def to_latex(self):
        sep =r'\quad |\quad '
        inner = '\\\\\n'.join(rf'{left} &\longrightarrow\quad {sep.join(w.to_latex() for v,w in rules)}' for left,rules in self.byVar.items())
        return '\\begin{align*}\n'+ inner +'\n\\end{align*}\n'
    def _repr_latex_(self):
        return self.to_latex()

    class Property ( namedtuple('Property',('vars','rules')) ) :
        pass

    def __init__ (self,terminals, axiom, rules_by_var) :

        assert axiom in rules_by_var.keys(), 'axiom {axiom} must have at least on rule'
        assert EOD not in terminals, 'EOD symbol "{EOD}" is reserved'
        assert EOD not in rules_by_var.keys(),'EOD symbol "{EOD}" is reserved'

        # main  definition attributes :
        self.terminals = list(terminals)
        self.terminals.sort()
        self.axiom = axiom
        self.byVar = rules_by_var  # dict of rulesets, indexed by var. e.g {'E':{('T','+','E'),('T')},'T':{('F','*','T'),('F')}, 'F':{'(E)',('x')}}

        # computed properties
        self._eps_prod =  self._compute_eps_prod()  # epsilon-productive vars and rules (type Property)
        self._productive = self._compute_prod()  # productive vars and rules (type Property)
        self._accessible = self._compute_accessible() # set of variables

        # waiting properties (computed when needed)
        self._prem = None
        self._suiv = None

        self._ll1_computed = False
        self._ll1_table = None
        self.ll1_error = None

        self._lr0 = None

    def getVars(self) -> Iterable[Symbol]:
        return self.byVar.keys()

    @property
    def variables(self) -> set[Symbol]:
        return set(self.byVar.keys())
    @property
    def rules(self) -> Iterable[Rule] :
        for ruleset in self.byVar.values() :
            for r in ruleset :
                yield r
    @property
    def eps_prod(self):
        return self._eps_prod
    @property
    def productive(self):
        return self._productive
    @property
    def accessible(self) -> set[Symbol]:
        return self._accessible
    @property
    def starting_rule(self) -> Rule :
        return Rule(GRAMMAR_START, Word(self.axiom))

    def isReduced(self) -> bool :
        """
             predicate "is this grammar reduced ?"
        """
        return len(self.productive.vars) == len(self.byVar) and len(self.accessible) == len(self.byVar)

    def isVar(self, name) -> bool :
        """
            name : str
            predicate " does name represent a variable symbol ? "
        """
        return name in self.byVar.keys()

    @property
    def prem(self) -> dict[Symbol, set[Symbol]]: # dict[Symbol | Rule, set[Symbol]]:
        return self.first

    @property
    def first(self) -> dict[Symbol,set[Symbol]] : #dict[Symbol|Rule,set[Symbol]] :
        """
            "First" sets (dict of sets, indexed by variables and rules)
            e.g.
                prem[myvar] : set of terminal symbols Prem(myvar)
                prem[somerule] : Prem (right part of the rule)
        """
        if self._prem is None :
            self._compute_premiers()
        return self._prem

    @property
    def suiv(self) -> dict[Symbol, set[Symbol]]:
        return self.follow

    @property
    def follow(self) -> dict[Symbol,set[Symbol]]:
        """
            "Follow" sets (dict of sets, indexed by variables)
            e.g.
                suiv[myvar] : set of terminal symbols Suiv(myvar)
        """
        if self._suiv is None :
            self._compute_suivants()
        return self._suiv

    def ll1_table(self) -> LL1Table :
        """
            returns LL(1) table (LL1Table instance)
            or None if grammar is not LL(1)
        """
        if self._ll1_computed :
            return self._ll1_table
        # else try to compute LL(1) parser
        if not self.isReduced() :
            raise Exception('grammar should be reduced first')
        try :
            self._ll1_table = LL1Table(self)
        except ConflictError as e :
            self.ll1_error = str(e)  # self._ll1_table remains None
        finally:
            self._ll1_computed = True
            return self._ll1_table

    def isLL1(self) -> bool :
        """
            predicate : "is this grammar LL(1) ?"
        """
        return self.LL1_table() is not None


    def _compute_eps_prod(self):
        return self._compute_closure(
            (lambda r: not r.right ),
            (lambda r, found: all( x in found for x in r.right) )
        )

    def _compute_prod(self):
        return self._compute_closure(
            (lambda r: all(not self.isVar(x) for x in r.right)),
            (lambda r, found: all(not self.isVar(x) or x in found for x in r.right))
        )

    def _compute_closure(self, select_init, select_step):
        """
            returns (variables set, rules set)
            select_init : function(Rule):bool  select initial rules
            select_step : function(Rule, found vars set) : select new Rules
        """
        rules = set(self.rules);
        result = Grammar.Property(set(), set())
        rules_selection = {r for r in rules if select_init(r)}
        new = {v for v, w in rules_selection}
        rules -= rules_selection
        # print(f'rules selction {rules_selection}')
        while new:
            result.vars.update(new)
            result.rules.update(rules_selection)
            rules_selection = {r for r in rules if select_step(r, result.vars)}
            # print(f'rules selction {rules_selection}')
            new = {v for v, w in rules_selection if v not in result.rules}
            rules -= rules_selection
        result.rules.update(rules_selection)
        return result

    def _compute_accessible(self) ->set[Symbol]:
        result = {self.axiom}
        todo = {self.axiom}
        while todo:
            v = todo.pop()
            for r in self.byVar[v]:
                for x in r.right:
                    if self.isVar(x) and x not in result:
                        result.add(x)
                        todo.add(x)
        return result

    def reduce(self) :
        """
            reduce this grammar  (/!\ modifies current grammar)
        """
        if not self.isReduced() :
            productive_vars, productive_rules = self.productive
            unproductive_vars = self.byVar.keys() - productive_vars

            if unproductive_vars :
                # print(f'prod vars{productive_vars}')
                # print(f'unprod vars{unproductive_vars}')
                # print(f'prod rules{productive_rules}')
                for var in self.byVar.keys() :
                    # self.byVar[var] &=  productive_rules
                    self.byVar[var] = dict((r,None) for r in self.byVar[var] if r in productive_rules)
                for v in unproductive_vars :
                    self.byVar.pop(v)
                if self.axiom not in self.byVar :
                    raise Exception('Grammaire non productive')

            inaccessibles = self.byVar.keys() - self._compute_accessible()
            for v in inaccessibles :
                self.byVar.pop(v)

            # update properties :
            self._eps_prod =  self._compute_eps_prod()    # epsilon-productive vars and rules (type Property)
            self._productive = self._compute_prod()  # productive vars and rules (type Property)
            self._accessible = self._compute_accessible()  # set of variables

    def __lfp(self, sets, supersets):
        """ complete sets using least fixed point algo (lfp)
            /!\ side effect : sets will be modified
        """
        modified = {v for v, content in sets.items() if content}
        while modified:
            v = modified.pop()
            for sup_set in supersets[v]:
                old_size = len(sets[sup_set])
                sets[sup_set].update(sets[v])
                if (len(sets[sup_set]) > old_size):  # set has grown
                    modified.add(sup_set)

    def _compute_premiers(self):
        """
            calcul des ensembles "premiers" pour les variables et les règles
        """
        # ensembles premiers (pour les variables);
        #   clé : variable valeur : symbole
        prem = {var: set().copy() for var in self.getVars()}
        # relation d'inclusion entre ensembles premiers (pour les variables);
        #   clé : variable,  valeur : variable ou règle
        supersets = {var: set().copy() for var in self.getVars()}

        for r in self.rules:
            prem[r] = set()
            supersets[r] = set()
            for x in r.right:  # /!\ break when prefix is no more eps-prod
                if self.isVar(x):  # x est une variable
                    supersets[x].add(r.left)
                    supersets[x].add(r)
                    if x not in self.eps_prod.vars:
                        break
                else:
                    prem[r.left].add(x)
                    prem[r].add(x)
                    break
        prem = dict(prem)
        supersets = dict(supersets)
        self.__lfp(prem, supersets)
        self._prem = prem

    def _compute_suivants(self):
        """
            calcul des ensembles "suivants" pour les variables
        """
        suiv = {var: set().copy() for var in self.getVars()}
        supersets = {var: set().copy() for var in self.getVars()}
        # suiv, supersets  = dict(), dict()
        # for var in self.getVars() :
        #     suiv[var], supersets[var] = set(), set()
        suiv[self.axiom] = {EOD}
        for r in self.rules:  # compute suiv base content and  suiv supersets
            suffix_eff = True
            e = set()  # cumul d'ensembles prem
            # print(f'rule {v}->{w}')
            for x in r.right[::-1]:  # visit in reverse order
                if self.isVar(x):  # x est une variable
                    suiv[x].update(e)
                    # print(f'added {e} if suiv({x})')
                    if suffix_eff:
                        supersets[r.left].add(x)
                        # print(f'suiv({v}) included in suiv({x})')
                    if x in self.eps_prod.vars:
                        e.update(self.prem[x])
                    else:
                        e = self.prem[x].copy()
                        suffix_eff = False
                else:  # x est une lettre terminale
                    e = {x}
                    suffix_eff = False

        self.__lfp(suiv, supersets)  # plus petit point fixe
        self._suiv = suiv

    def _context_follows(self, w: Word):
        eps = True
        current = set()
        follows = dict()
        for i,x in enumerate(reversed(w)) :
            if self.isVar(x) :
                follows[len(w)-i-1]= WordFollow(letters = current.copy(), eps = eps)
                if x not in self.eps_prod.vars :
                    current = self.prem[x].copy()
                    eps = False
                else :
                    current |= self.prem[x]
            else :
                current = {x}
                eps = False
        return follows

    @staticmethod
    def from_string(source)  :  # deprecated
        """
            Static method
            returns new Grammar instance
            source : multiline string containing context-free grammar definition. e.g :
                E -> E + T | T
                T -> T * F | F
                F -> * F  | ( E ) | i
            symbols must be separated by at least one blank
            the first used variable is regarded as axiom
        """
        return GrammarBuilder.from_string(source)

    def lr0_automaton(self) -> LR0Automaton : #| None:
        """
            returns LR(0) automaton (LR0Automaton)
        """
        if self._lr0 is not None :
            return self._lr0
        if not self.isReduced() :
            raise Exception('grammar should be reduced first')
        try :
            self._lr0 = LR0Automaton(self)
        finally:
            return self._lr0

    def isLR0(self) -> bool :
        return self._lr0.isLR0()

    def tableLR0(self,raise_exception = False) -> LRTable :
        return self._lr0.table('LR0',raise_exception)

    def tableSLR1(self,raise_exception = False) -> LRTable :
        return self._lr0.table('SLR1',raise_exception)

    def isSLR1(self) -> bool:
        return self._lr0.isSLR1()

    def isLALR1(self) -> bool :
        return self._lr0.isLALR1()


class GrammarBuilder() :  # static methods
    @staticmethod
    def _parse_line(line) -> list[Rule]:
        """
            l : stripped string
            returns list of Rules (Rules have same left side)
        """
        parts = PATTERNS.ARROW.split(line)
        assert len(parts) == 2, f"{line} : Error : line must contain exactly on arrow sign"
        left, right = Symbol(parts[0]), (Word.fromString(s) for s in PATTERNS.SEPARATOR.split(parts[1]))
        return [Rule(left, word) for word in right]

    @staticmethod
    def from_rules(rules : Iterable[Rule]) -> Grammar:
        """
            Static method
            Creates grammar from rules
            rules : Iterable of rules.
                    left variable of the first rule is regarded as axiom
            returns Grammar instance
        """
        dico = defaultdict(lambda : defaultdict(lambda : None)) # rulesets dictionary, indexed by left part
        # dico = defaultdict(set)  # rulesets dictionary, indexed by left part
        symbols = set()  # symbols (terminals or variables)
        for rule in rules :
            # dico[rule.left].add(rule)
            dico[rule.left][rule]
            symbols.update(rule.right)
        dico = dict((k,dict(v)) for k,v in dico.items())  # dico without default value
        forbid = symbols & { EOD }
        assert not forbid, f"forbidden symbol : {forbid}"
        return Grammar(
            terminals = symbols - dico.keys(),
            axiom = next(iter(dico)),
            rules_by_var = dico
        )

    @staticmethod
    def from_string(source : str) -> Grammar :
            """
                Static method
                returns new Grammar instance
                source : multiline string containing context-free grammar definition. e.g :
                    E -> E + T | T
                    T -> T * F | F
                    F -> * F  | ( E ) | i
                symbols must be separated by at least one blank
                the first used variable is regarded as axiom
            """
            lines = filter( bool,  map(str.strip, source.splitlines()) ) #iterator through non empty stripped lines
            rules = itertools.chain.from_iterable(map(GrammarBuilder._parse_line, lines)) #iterator through rules
            return GrammarBuilder.from_rules(rules)

    @staticmethod
    def from_semantic_dict(definitions : dict) -> tuple[Grammar, dict]:
        """
            create Grammar from dictionary :
                key : grammar rules
                value : callable (synthetised attribute computation)

        """
        dico = { rule : definitions[line] for line in definitions for rule in GrammarBuilder._parse_line(line)}
        grammar = GrammarBuilder.from_rules(dico)
        return (grammar,DictSemantic(dico))
