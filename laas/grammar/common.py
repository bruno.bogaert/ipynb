"""
    Context-free grammars.
    (cc) CC BY-NC-SA Creative Commons -  Bruno.Bogaert@univ-lille.fr
"""
import re
from collections import namedtuple
from types import SimpleNamespace
from typing import Callable
from typing import Any

# PATTERNS
PATTERNS = SimpleNamespace()
PATTERNS.SPACE = re.compile('\s')
PATTERNS.ARROW = re.compile('\s*(?:->|\N{Rightwards Arrow})\s*')
PATTERNS.SEPARATOR = re.compile('\s*[|]\s*')

_latex_translate_dict = {ord(x): '\\'+x for x in r'"%$_{}#&'}
_latex_translate_dict.update({ ord('\\'):'\\backslash{}', ord('~'):'\\textasciitilde{}',
                               ord('-'):'\\text{-}',ord('+'):'\\text{+}'})


class Symbol (str) :
    """ Non empty space free string
    """
    def __new__(cls, s):
        assert s!='' and PATTERNS.SPACE.search(s) is None, f"{s} : invalid symbol"
        return super(Symbol, cls).__new__(cls, s)
    def to_latex(self):
        return self.translate(_latex_translate_dict)

# reserved symbols
GRAMMAR_START = Symbol('_start')
EOD = Symbol('#')
EPSILON = Symbol('\N{Greek Small Letter Epsilon}') #'ε'


class Word (tuple) :
    """ Sequence of Symbol(s)
        constructor : Word (*symbols)
    """
    def __new__(cls, *parts):
        assert all(isinstance(x,Symbol) for x in parts), f"{parts} : each word component must be a symbol"
        # return super(Word, cls).__new__(cls, tuple(parts))
        return super().__new__(cls, filter(lambda s : s!=EPSILON, parts))

    def __str__(self):
        return  EPSILON if self == () else ' '.join(self)
    def __repr__(self):
        return 'Word' + super().__repr__()

    def to_latex(self):
        return '\\varepsilon{}' if self == () else '\\ '.join(symbol.to_latex() for symbol in self)

    @staticmethod
    def fromString(s) :
        s = s.strip()
        return Word( *(Symbol(x) for x in s.split()) ) if s != '' else Word()

class Rule (namedtuple('Rule',('left','right'))) :
    """ Context free grammar rule
        left : Symbol
        right : Word
    """
    def __new__(cls, left, right):
        assert isinstance(left,Symbol)
        assert isinstance(right,Word)
        return super(Rule, cls).__new__(cls, left, right)
    def __repr__(self):
        return f'{self.__class__.__name__}({self})'
    def __str__(self):
        return f'{self.left} \N{Rightwards Arrow} {self.right}'
    @classmethod
    def fromString(cls,line):
        parts = PATTERNS.ARROW.split(line)
        assert len(parts) == 2, f"{line} : Error : line must contain exactly on arrow sign"
        left, right = Symbol(parts[0]), tuple(Word.fromString(s) for s in PATTERNS.SEPARATOR.split(parts[1]))
        assert len(right) == 1
        return Rule(left, right[0])


class ConflictError(Exception) :
    pass

class ParseError(Exception) :
    pass

class Semantic():
    """ bind semantic actions to rules
    """
    def start_synth(self) -> Callable [[Any,SimpleNamespace,SimpleNamespace], Any]:
        return lambda p, run, _ : p[0]

    def rule_synth(self, rule : Rule) -> Callable [[list[Any],SimpleNamespace, SimpleNamespace], Any]:
        return lambda p, run, _ : f'{run.var} ( {" ".join(map(str,p))} )'

    def token_semantic(self) -> Callable [[Any],Any]:
        return lambda token : token.value


class Parser():

    def parse(self, data, verbose=False, semantic : Semantic = None):
        """
            data : Iterable[str] ou Iterable[Symbol]
        """
        wrap = lambda x : SimpleNamespace(type=x,value=x)
        return self.parse_tokens(map(wrap, iter(data)), verbose=verbose, semantic=semantic)

    def parse_tokens(self, tokens, verbose=False, semantic : Semantic = None):
        """
            tokens : Iterable[Token] (without End Of Data token)
            Token :  any class with 'type' and 'value' attributes
        """
        pass


