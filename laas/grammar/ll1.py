"""
    Context-free grammars. LL(1) parsing tools
    (cc) CC BY-NC-SA Creative Commons -  Bruno.Bogaert@univ-lille.fr
"""
import itertools

from .semantic import RuleRun
from .common import EOD
from .common import GRAMMAR_START
from .common import Symbol
from .common import Word
from .common import ConflictError
from .common import ParseError
from .common import Parser
# from common import Word
from .common import Rule
from types import SimpleNamespace
from .semantic import Semantic


class LL1Table():
    @property
    def table(self):
        """
            table as dictionary :
                key : pair (variable, terminal)
                value : word (right part of a rule)

            No default value : raise exception if (variable, terminal) cell is empty
            Use for instance table.get( key, None ) to get None default value
        """
        return self._table

    def __init__(self, g):
        """
            g: Grammar
        """
        self._grammar = g
        table = dict()  # table LL(1). clé : (variable, lettre terminale)
        for r in g.rules:
            for x in g.prem[r]:
                if (r.left, x) in table:
                    raise ConflictError(f'conflit Premiers {(r.left, x)}: {r.right} vs {table[(r.left, x)]}')
                table[(r.left, x)] = r.right
        for r in g.eps_prod.rules:
            for x in g.suiv[r.left]:
                if (r.left, x) in table:
                    raise LL1Table.ConflictError(f'conflit Suivants {(r.left, x)}: {r.right} vs {table[(r.left, x)]}')
                table[(r.left, x)] = r.right
        self._table = table

    def get(self, var, term ):
        """
            get cell content for key (var,term)
            var : variable symbol
            term : terminal symbol

            Returns Word or None
        """
        return self.table.get((var,term),None)

    def get_as_rule(self, var, term ):
        """
            get cell content for key (var,term)
            var : variable symbol
            term : terminal symbol

            Returns Rule or None
        """
        word = self.get(var,term)
        return Rule(Symbol(var), word) if word is not None else None

    def parser(self):
        return LL1Parser(self)

    def to_pandas(self):
        """
            Returns table as pandas.DataFrame
        """
        import pandas
        lines = list(self._grammar.terminals) + [EOD]
        f = pandas.DataFrame(dict.fromkeys(self._grammar.getVars(), [""] * len(lines)), index=lines)
        for (v, x), w in self.table.items():
            # f[v][x] = f'{v}->{" ".join(w)}'
            f[v][x] = str(Rule(v,w))
        return f

    def to_latex(self):
        """
            Returns table as latex table
            (use pandas DataFrame)
        """
        return self.to_pandas().to_latex(column_format='|l|' + 'c|' * len(f.columns))

    def to_html(self):
        """
            Returns table as HTML table
            (use pandas DataFrame)
        """
        styler = self.to_pandas().style
        td = {'selector':'td','props': 'border : solid 0.8pt; text-align:left'}
        th = {'selector':'th','props': 'text-align:center'}
        return styler.set_table_styles([td,th]).to_html()

    def _repr_html_(self):
        return self.to_html()

    def to_markdown(self):
        """
            Returns table as markdown table
            (use pandas DataFrame)
        """
        return self.to_pandas().to_markdown()



class LL1Parser(Parser) :
    def __init__(self, table: LL1Table ):
        self._table = table

    def parse_tokens(self, tokens, verbose=False, semantic : Semantic = None):
        """
            tokens : Iterable[Token] (without End Of Data token)
            Token :  any class with 'type' and 'value' attributes
        """
        table = self._table
        grammar = table._grammar
        if semantic :
            runs = RunManager(semantic)
        input = itertools.chain(tokens, [SimpleNamespace(type=EOD,value=None)])
        current = next(input)
        stack = [grammar.axiom]
        verbose and print(f'stack:{stack}, current: {current.type}')
        while stack :
            symbol = stack.pop()
            verbose and print(f'stack:{stack}, symbol:{symbol}, current: {current.type}')
            if grammar.isVar(symbol) :
                rule = table.get_as_rule(symbol,current.type)
                if rule is not None :
                    verbose and print(f"apply {rule}")
                    stack.extend( reversed (rule.right) )
                    semantic and runs.create_run(rule)    # ==> semantic
                    verbose and print(f'stack:{stack}, current: {current.type}')
                else :
                    raise ParseError('mot incorrect')
            elif symbol == current.type:
                verbose and print("shift")
                semantic and runs.add_token_result(current) # ==> semantic
                current = next(input)
            else:
                raise ParseError(f'mot incorrect, waiting {symbol}')
        if current.type != EOD:
            raise ParseError('mot incorrect waiting EOD')
        if semantic :
            return runs.synthetised  # ==> semantic
        else :
            return True


class RunManager() :
    def __init__(self, semantic : Semantic):
        self._stack = [RuleRun(semantic = semantic, rule = Rule(GRAMMAR_START,Word(Symbol('_'))))]
        self._token_semantic = semantic.token_semantic()
        self._semantic = semantic

    def create_run(self, rule:Rule):
        created = RuleRun(rule=rule, semantic=self._semantic)
        if created.isComplete() :
            self.add_result(created.synthetised)
        else :
            current = self._stack[-1]
            index = len(current._p)
            if getattr(current,'down',False) and index in current.down :
                 created.run.inherited = current.down[index](current.run,current.namespace)
            self._stack.append(created)

    def add_token_result(self,token):
        self.add_result(self._token_semantic(token))

    def add_result(self, value):
        self._stack[-1].add_result(value)
        while len(self._stack)>=2 and self._stack[-1].isComplete() :
            run = self._stack.pop()
            self._stack[-1].add_result(run.synthetised)

    @property
    def synthetised(self):
        if len(self._stack) != 1 :
            raise Exception(f'erreur {len(self._stack)} runs')
        return self._stack[0].synthetised







# def arbre(self, data):
    #     table = self._table
    #     stack = [self._grammar.axiom]
    #     data = data + EOD
    #     current_pos = 0
    #     current = data[current_pos]
    #     stackexp = []
    #     begin_exp = ''
    #     num = 0
    #     while stack != []:
    #         print(stack)
    #         ini = stack.pop();
    #         # print(ini,current)
    #         if ini.isupper() and (ini, current) in table:
    #             w = table[(ini, current)]
    #             print(f"apply {ini}->{w}")
    #             begin_exp += f' [.{ini}'
    #             stack.append(' ]')
    #             for c in w[::-1]:
    #                 stack.append(c)
    #         elif ini == current:
    #             # print("shift")
    #             # begin_exp += f' {ini}'
    #             begin_exp += f' \\node[old] (x_{num}) {{{ini}}};'
    #             num += 1
    #             current_pos += 1
    #             current = data[current_pos]
    #         else:
    #             raise Exception('mot incorrect')
    #
    #         while stack != [] and stack[-1] == ' ]':
    #             begin_exp += ' ]'
    #             stack.pop();
    #         if stack != []:
    #             exp = begin_exp + f' \\node[currentvar] {{{stack[-1]}}};' + ' '.join(stack[-2::-1])
    #             stackexp.append(exp)
    #     if current != EOD:
    #         raise Exception('mot incorrect waiting eod')
    #     return stackexp

# def tabular_dict(d) :
#     cols = sorted({ entry[0] for entry in d.keys() })
#     lines = sorted({ entry[1] for entry in d.keys() })
#     print(lines)
#     print(cols)
#     format_parm_action = lambda p : f"${p[0]}\\to {' '.join(p[1])}$" if isinstance(p,tuple) else str(p)
#     format_action = lambda act : f'{act[0]} {format_parm_action(act[1])}'
#     inner= (r'\\\hline '+'\n').join([f'{l}&'+'&'.join([format_action(d.get((c,l),('Ab',''))) for c in cols]) for l in lines ])
#     inner += r'\\\hline '+'\n'
#     header = '&'+'&'.join([str(x) for x in cols])+r'\\\hline'
#     all= f'\\begin{{tabular}}{{l|{"c|"*len(cols)}}}\n{header}\n{inner}\n\end{{tabular}}'
#     return all.replace(EOD,'\#')
#     #return [(d.get((c,l),'Abort') for c in cols) for l in lines]

        # def translate_tokens(self, tokens, verbose=True):  # parse and translate
        #     """
        #         data : Iterable[str] ou Iterable[Symbol]
        #     """
        #     # default_synth = lambda ctx : ','.join(ctx.res)
        #     default_synth = lambda ctx: f"{ctx.var}< {' '.join(ctx.res)} >"
        #
        #     EOR = Symbol('_EOR_')
        #     table = self._table
        #     grammar = table._grammar
        #     input = itertools.chain(tokens, [SimpleNamespace(type=EOD, value=None)])
        #     current = next(input)
        #     stack = [grammar.axiom]
        #     contexts = [SimpleNamespace(res=[], synth=None)]
        #     verbose and print(f'stack:{stack}, current: {current.type}')
        #     while stack:
        # symbol = stack.pop();
        #     verbose and print(f'stack:{stack}, symbol:{symbol}, current: {current.type}')
        #     if symbol == EOR : #end of rule
        #         context = contexts.pop()
        #         result = context.synth(context)
        #         contexts[-1].res.append(result)
        #     elif grammar.isVar(symbol) :
        #         w = table.get(symbol,current.type)
        #         if w is not None :
        #             verbose and print(f"apply {symbol}->{w}")
        #             stack.append(EOR)
        #             stack.extend( reversed (w) )
        #             contexts.append(SimpleNamespace(res=[],var=symbol,synth = default_synth))
        #             verbose and print(f'stack:{stack}, current: {current.type}')
        #         else :
        #             raise ParseError('mot incorrect')
        #     elif symbol == current.type:
        #         verbose and print("shift")
        #         contexts[-1].res.append(current.value)
        #         current = next(input)
        #     else:
        #         raise ParseError(f'mot incorrect, waiting {symbol}')
        #
        # if current.type != EOD:
        #     raise ParseError('mot incorrect waiting EOD')
        # if len(contexts) != 1 :
        #     raise Exception(f'Pb pile contextes : {contexts}')
        #
        # return contexts[-1].res[0]
