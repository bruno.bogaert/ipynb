"""
    Context-free grammars.

    (cc) CC BY-NC-SA Creative Commons -  Bruno.Bogaert@univ-lille.fr
"""
from typing import Callable
from typing import Any
from types import SimpleNamespace
from .common import *
import itertools
from itertools import chain
from textwrap import dedent
import html
import json

class ParmsList (list) :
    def __init__(self, w : Word):
        super().__init__()
        positions = dict()
        count = dict()
        for index,symbol in enumerate(w) :
            k = f'{symbol}{count.get(symbol,0)}'
            if symbol in positions or k in count :
                raise Exception(f'incompatible variable name {symbol}')
            positions[k] = index
            count[symbol] = 1 + count.get(symbol,0)
        for symbol in count :
            if count[symbol]==1 :
                positions[symbol] = positions[f'{symbol}0']
        self._word_length = len(w)
        self._positions = positions

    def __getattr__(self, attr):
        return self[self._positions[attr]]

    def __setattr__(self, attr, value):
        if attr in ('_positions','_word_length') :
            super().__setattr__(attr, value)
        else :
            self[self._positions[attr]] = value

    def append(self, __object) -> None:
        if len(self) == self._word_length :
            raise Exception('list is full')
        super().append( __object)

    def extend(self, __iterable) -> None:
        _=(* map(self.append,__iterable),)


class ParmsValues (ParmsList) :

    def __setitem__(self, key, value):
        """
            Forbidden.
            NB : to add new values, use append or extend
        """
        raise Exception('values are read-only')



default_init_f =  lambda p, run, ctx: p[0]

class SimpleSemantic(Semantic):
    """
        same semantic for any Rule
    """
    # default_init_f = lambda run, ctx: run.res[0]
    def __init__(self, synth_function, start_function=default_init_f):
        self._synth_function = synth_function
        self._start_function = start_function

    def start_synth(self):
        return self._start_function

    def rule_synth(self, rule  : Rule):
        return self._synth_function

class DictSemantic(Semantic):
    """
        based on dict of functions indexed by rule
    """
    def __init__(self, sem_dict, start_function=default_init_f):
        to_rule = lambda obj : obj if type(obj) is Rule else  Rule.fromString(obj)
        self._sem_dict = {to_rule(k):v for k,v in sem_dict.items()}
        self._start_function = start_function

    def start_synth(self):
        return self._start_function

    def rule_synth(self, rule : Rule):
        return self._sem_dict[rule]

#AST_expression = SimpleSemantic(lambda run, ctx : f"{run.var}<{' '.join(run.res)}>")

class TikzTreeSemantic(SimpleSemantic) :
    @staticmethod
    def tikz_node(p,run,_) :
        if not p :
            p = [r'\node[eps]{$\varepsilon$};']
        return rf"[.\node[var]{{{run.var}}}; {' '.join(p)} ]"
    @staticmethod
    def tikz_init(p,_):
        return f'\Tree {p[0]}'

    def __init__(self):
        super().__init__(synth_function=__class__.tikz_node, start_function =  __class__.tikz_init)

class ForestSemantic(Semantic) :
    @staticmethod
    def forest_node(p,run,_) :
        if not p :
            p = [r'[$\varepsilon$, eps]']
        return rf"[{run.var} {' '.join(p)} ]"
    @staticmethod
    def forest_init(p,run,_):
        return rf'\begin{{forest}} {p[0]} \end{{forest}}'
    @staticmethod
    def forest_token(token : Any) -> str:
        return f'[{token.type}, token]'

    def start_synth(self) -> Callable [[Any, SimpleNamespace], Any]:
        return __class__.forest_init

    def rule_synth(self, rule : Rule) -> Callable [[list[Any],SimpleNamespace, SimpleNamespace], Any]:
        return __class__.forest_node

    def token_semantic(self) -> Callable [[Any],Any]:
        return __class__.forest_token

class GraphvizTree(Semantic) :
    escape_dict = {ord(x): '\\' + x for x in r'"\{}|'}

    def __init__(self, token_display='type', with_word : bool = False, graph_attributes={}):
        # def __init__(self, token_display: str | callable = 'type'):
        self.node_count = 0
        if token_display == 'type' :
            self._token_display = lambda tok : tok.type
        elif token_display == 'value' :
            self._token_display = lambda tok : str(tok.value)
        elif token_display == 'both' :
            self._token_display = lambda tok : f'{tok.value} {tok.type}'
        else :
            self._token_display = token_display

        self._graph_attributes = {'margin':0, 'bgcolor':'transparent'}| graph_attributes
        self._leaf_list =[]
        self._token_list =[]
        self._with_word = with_word

    def _create_id(self):
        node_id = f'node{self.node_count}'
        self.node_count += 1
        return node_id

    def _create_node(self, label, node_id=None, raw_html=False, **attributes):
        if node_id is None:
            node_id = self._create_id()
        label = f'<{label}>' if raw_html else label.translate(GraphvizTree.escape_dict)
        attr_str = ' '.join((f'{key}="{value}"' for key, value in attributes.items()))
        node_def = f'{node_id}[label="{label}",{attr_str}]'
        return (node_id, node_def)
    def _create_word_node(self, content, node_id = None, **attributes):
        if node_id is None :
            node_id = self._create_id()
        label = '|'.join(f'<{i}>{s.translate(GraphvizTree.escape_dict)}' for i, s in enumerate(content))
        attr_str = ' '.join((f'{key}="{value}"' for key,value in attributes.items()))
        node_def = f'{node_id}[label="{label}" shape="record" {attr_str}]'
        return (node_id, node_def)

    def gv_rule(self,p,run,_) :
        if not p :
            eps_id, eps_def = self._create_node(label=EPSILON,shape='none')
            p = [(eps_id,(eps_def,),(),())]
        node_id, node_def = self._create_node(label=run.var,shape='box')
        edges = (f'{node_id}->{res[0]}' for res in p)
        all_nodes = chain((node_def,), chain.from_iterable(res[1] for res in p))
        all_edges = chain(edges, chain.from_iterable(res[2] for res in p))
        if len(p)>1 :
            chilren_list = ','.join(res[0] for res in p)
            constraint = (f'{{rank=same;{chilren_list}}}',)
        else :
            constraint = ()
        all_constraints = chain( constraint ,chain.from_iterable(res[3] for res in p))

        return (node_id, all_nodes, all_edges, all_constraints)

    def gv_init(self,p,run,_):
        attr_str = ';'.join((f'{key}="{value}"' for key, value in self._graph_attributes.items()))
        res = ['digraph arbre {',' node [width=0.1,height=0.05]',' edge [arrowhead="none"]', attr_str]
        res.extend(chain(p[0][1],p[0][2],p[0][3]))
        if self._with_word :
            word_id, word_def = self._create_word_node(content = (str(t.value) for t in self._token_list))
            res += [word_def,'edge [minlen=3,style=dashed,arrowhead=onormal,arrowsize=0.7]']
            res.extend(f'{leaf_id}->{word_id}:{index}:n  ' for index,leaf_id in enumerate(self._leaf_list) )
        res += ['}']
        return '\n'.join(res)


    def gv_token(self, token : Any) :
        label = self._token_display(token)
        node_id, node_def = self._create_node(label=label,shape='box',style='dashed,rounded,filled')
        self._leaf_list.append(node_id)
        self._token_list.append(token)
        return (node_id, (node_def,), (),())

    def start_synth(self) -> Callable [[Any, SimpleNamespace], Any]:
        return self.gv_init

    def rule_synth(self, rule : Rule) -> Callable [[list[Any],SimpleNamespace, SimpleNamespace], Any]:
        return self.gv_rule

    def token_semantic(self) -> Callable [[Any],Any]:
        return self.gv_token




# class TikzSemanticN(Semantic) :
#     components = {
#         'forest' : {
#             'empty_node' : r'[$\varepsilon$]',
#             'root_func' : str,
#             'init_func' : lambda content : rf'\begin{{forest}} {content} \end{{forest}}'
#         },
#         'tikz-qtree': {
#             'empty_node' : (r'\node[eps]{$\varepsilon$};',) ,
#             'root_func' :  lambda v : rf'.\node[var]{{{v}}}',
#             'init_func': lambda content: rf'\Tree {content}'
#         }
#     }
#     def tikz_node(self,run,_) :
#         compo = self.__class__.components[self._mode]
#         children_trad = ' '.join(run.res) if run.res else compo['empty_node']
#         root_trad = compo['root_func'](run.var)
#         return  rf"[{root_trad} {children_trad}]"
#
#     def tikz_init(self,run,_):
#         compo = self.__class__.components[self._mode]
#         return compo['init_func'](run.res[0])
#
#     def rule_synth(self, rule=None):
#         if rule is None :
#             return SimpleNamespace(synth=self.tikz_init)
#         else :
#             return SimpleNamespace(synth=self.tikz_node)
#
#     def token_semantic(self):
#         return lambda v : f'[{v}]'
#
#     def __init__(self, mode):
#         assert mode in ['tikz-qtree','forest']
#         self._mode = mode
#

tikz_tree = TikzTreeSemantic()
#
class RuleRun():
    # default_synth = lambda run, ctx : f"{run.var}<{' '.join(run.res)}>"
    # default_synth = lambda run, ctx: ','.join(run.res)
    def __init__(self, semantic : Semantic, rule : Rule ):
        self.namespace = SimpleNamespace()
        self._p = ParmsValues(rule.right)
        self.run = SimpleNamespace(len=len(rule.right), var=rule.left, rule = rule )
        if rule.left == GRAMMAR_START :          # start context
            self.synth_func = semantic.start_synth()
        else :
            self.synth_func = semantic.rule_synth(rule)
        if self.run.len == 0 : # epsilon rule
            self._resolve()

    def _resolve(self):
        if not self.isComplete() :
            raise Exception('Run is not complete')
        self.synthetised = self.synth_func(self._p,self.run,self.namespace)

    def isClosed(self):
        return getattr(self,'synthetised') is  not None

    def isComplete(self):
        return self.run.len == len(self._p)

    def add_result(self,value):
        assert not self.isComplete()
        self._p.append(value)
        if self.isComplete() :
            self._resolve()

    def add_results(self, * values):
        for v in values :
            self.add_result(v)



#
# class ParmsFuncs (ParmsList) :
#     def __init__(self, w : Word):
#         super().__init__(w)
#         self.extend([None]*len(w))
#
#
#
#
# p = ParmsValues("a b a c b d a e f e ".split())

